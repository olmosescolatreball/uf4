const lletraAbuscar = "z";
const delay = async (t) => new Promise((resolve) => setTimeout(resolve, t));

const buscalletra = async (cadena, index, lletraAbuscar) => {
  await delay(500);
  if (cadena[index] == lletraAbuscar) {
    await console.log(cadena, index, "cadena trobada");
  } else {
    await console.log("lletra no trobada");
  }
};

async function busca(text) {
  console.log(text);
  for (var i = 0; i < text.length; i++) {
    await buscalletra(text, i, lletraAbuscar);
  }
}

function doTask() {
  busca(document.getElementById("text1").value);
  busca(document.getElementById("text2").value);
  busca(document.getElementById("text3").value);
}
